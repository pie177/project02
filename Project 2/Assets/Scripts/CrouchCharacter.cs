﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrouchCharacter : MonoBehaviour {

    [HideInInspector]
    public float cameraY;
    [HideInInspector]
    // Use this for initialization
    void Start() {
        cameraY = this.gameObject.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.C))
        {
            if (cameraY > 0.5f * cameraY)
            {
                cameraY -= 0.5f;
            }
        }
    }
}
