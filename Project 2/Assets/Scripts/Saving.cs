﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using UnityEngine.SceneManagement;

public class Saving : MonoBehaviour {

    public static Saving save;

    public int health;
    public int lives;
    public Vector3 playerPosition;
    public int ammo;

    void Awake()
    {
        if(save == null)
        {
            DontDestroyOnLoad(gameObject);
            save = this;
        }
        else if(save != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        Debug.Log(health);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            Save();
        }

        if(Input.GetKeyDown(KeyCode.D))
        {
            health -= 10;
            Debug.Log(health);
        }
    }

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

        PlayerData info = new PlayerData();
        info.health = health;
        info.lives = lives;
        info.currentScene = SceneManager.GetActiveScene().name;

        bf.Serialize(file, info);
        file.Close();
    }
}
