﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author: Michael Nelson
//Class Snowball is the base standard ammoless weapon
public class Snowball : MonoBehaviour {

    //reload between shots
    public int reload;
    //The snowball to be thrown
    public GameObject bullet;
    //damage the snowball does
    public int damage;
    //checks if weapon is active
    public bool isActive = true;
    //Where the bullet spawns from
    public GameObject shootPoint;

    //Force at which the bullet moves
    public float shootForce = 10;

    //reload time being added up
    float reloadTime;

    //Ryan Edit
    public GameObject player;

	// Use this for initialization
	void Start ()
    {
        //set the timer so you can fire
        reloadTime = reload;
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Increase the timer over time
        reloadTime += Time.deltaTime;

        //Throw the snowball
        if (Input.GetMouseButtonDown(0) && isActive == true && reloadTime >= reload)
        {
            //Reset the timer
            reloadTime = 0;

            //Create a snowball, throw it, and apply damage where applicable
            GameObject tempBullet = Instantiate(bullet, shootPoint.transform.position,  Quaternion.identity);
            tempBullet.GetComponent<Projectile>().damage = damage;
            Rigidbody bulletRigidbody = tempBullet.GetComponent<Rigidbody>();
            bulletRigidbody.AddForce(player.transform.forward * shootForce * Time.deltaTime, ForceMode.Impulse);

            //Destroy it if it doesn't hit anything
            Destroy(tempBullet, 5f);
        }
	}
}
