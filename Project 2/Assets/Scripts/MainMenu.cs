﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// This will load the first scene
    /// </summary>
    public void _StartGame()
    {
        SceneManager.LoadScene("01_Level");
    }

    /// <summary>
    /// This will exit the application
    /// </summary>
    public void _QuitGame()
    {
        Application.Quit();
    }

    public void _GoBack()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void _Load()
    {
        if(File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
            PlayerData info = (PlayerData)bf.Deserialize(file);

            SceneManager.LoadScene(info.currentScene);
            Game_Controller.gameController.health = info.health;
            Game_Controller.gameController.lives = info.lives;
            Game_Controller.gameController.MoveCharacter(info.playerPos);

        }
    }
}
