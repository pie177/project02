﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_AI : MonoBehaviour {

    public float snowballRange;
    public float visionRange;
    public float snowballSpeed;
    public GameObject snowballSpawn;
    public int damage;


    public GameObject snowBall;
    [HideInInspector]
    public GameObject player;
    Animator anim;

    public int health;

    const string P_PLAYERVSISIBLE = "playerVisible";
    const string P_PLAYERINRANGE = "playerInRange";

	// Use this for initialization
	void Awake ()
    {
        player = GameObject.FindWithTag("Player");
        anim = transform.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        PlayerVisible();
        PlayerInRange();

        if(health <= 0)
        {
            Game_Controller.gameController.enemiesKilled++;
            Destroy(this.gameObject);
        }
	}

    public void PlayerVisible()
    {
        Ray ray = new Ray(transform.position, (player.transform.position - transform.position));
        RaycastHit hit;

        if (Physics.Raycast(ray.origin, ray.direction, out hit, visionRange))
        {
            if(hit.transform.CompareTag("Player"))
            {
                anim.SetBool(P_PLAYERVSISIBLE, true);
            }
            else
            {
                anim.SetBool(P_PLAYERVSISIBLE, false);
            }
        }
        else
        {
            anim.SetBool(P_PLAYERVSISIBLE, false);
        }
    }

    public void PlayerInRange()
    {
        float distance = Vector3.Distance(player.transform.position, transform.position);

        if(distance <= snowballRange)
        {
            anim.SetBool(P_PLAYERINRANGE, true);
            //InvokeRepeating("ThrowSnowballs", 1f, 3f);
        }
        else
        {
            anim.SetBool(P_PLAYERINRANGE, false);
        }
    }

    public void ThrowSnowballs()
    {
        Ray ray = new Ray(transform.position, (player.transform.position - transform.position));
        GameObject tempBall = Instantiate(snowBall, snowballSpawn.transform.position, Quaternion.identity);
        tempBall.GetComponent<Projectile>().damage = damage;
        tempBall.transform.LookAt(player.transform);
        Rigidbody ballRigidbody = tempBall.GetComponent<Rigidbody>();
        ballRigidbody.AddForce(tempBall.transform.forward * snowballSpeed * Time.deltaTime, ForceMode.Impulse);


        Destroy(tempBall, 10f);
    }

    public void StartThrowing()
    {
        InvokeRepeating("ThrowSnowballs",1f, 3f);
    }

    public void StopThrowing()
    {
        CancelInvoke("ThrowSnowballs");
    }

    private void OnCollisionEnter(Collision other)
    {
        if(other.transform.GetComponent<Projectile>() != null)
        {
            health -= other.transform.GetComponent<Projectile>().damage;
        }
    }
}
