﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author: Michael Nelson
//Class GunsWithAmmo is for all remaining weapons that use ammunition
public class GunsWithAmmo : MonoBehaviour {

    public GameObject player;

    //Max ammunition
    public int maxAmmo;
    //ammunition
    public int ammo;
    //Maximum clip
    public int maxClip;
    //reload between shots
    public int reload;
    //The snowball to be thrown
    public GameObject bullet;
    //damage the snowball does
    public int damage;
    //checks if weapon is active
    public bool isActive = false;
    //Where the bullet spawns from
    public GameObject shootPoint;
    //rate of fire
    public float fireRate;
    //ammo in clip
    public int clip;

    //Level the weapon becomes available
    public int levelAvailable;

    //Force at which the bullet moves
    public float shootForce = 2000;

    float reloadTime;
    float timeBetweenShot;



    // Use this for initialization
    void Start()
    {
        //Initiate all values to be ready to fire
        reloadTime = reload;
        timeBetweenShot = fireRate;
        clip = maxClip;
        ammo -= maxClip;

    }

    // Update is called once per frame
    void Update()
    {
        //Add time to the time between shots
        timeBetweenShot += Time.deltaTime;
        reloadTime += Time.deltaTime;

        //Launch the snowball
        if (Input.GetMouseButton(0) && clip > 0 && reloadTime > reload && timeBetweenShot > fireRate && isActive)
        {
            //Reduce ammo by 1
            clip--;
            //Reset timer
            timeBetweenShot = 0;
            //Create a bullet then shoot it and apply damage where necessary
            GameObject tempBullet = Instantiate(bullet, shootPoint.transform.position, Quaternion.identity);
            tempBullet.GetComponent<Projectile>().damage = damage;
            Rigidbody bulletRigidbody = tempBullet.GetComponent<Rigidbody>();
            bulletRigidbody.AddForce(player.transform.forward * shootForce * Time.deltaTime, ForceMode.Impulse);

            //Destroy bullet if it hits nothing
            Destroy(tempBullet, 5f);
        }

        //Reload the weapon
        if(Input.GetKeyDown(KeyCode.R))
        {
            //Reset reload timer
            reloadTime = 0;

            //Return the clip to the ammo pool
            ammo += clip;

            //Excess of ammo
            if (ammo >= maxClip)
            {
                //Return a full clip from the ammo pool
                clip = maxClip;
                ammo -= maxClip;
            }

            //No ammo
            else if(ammo <= 0)
            {
                //Empty the gun
                ammo = 0;
                clip = 0;
            }

            //Nearly out of ammo
            else if(ammo > 0 && ammo < maxClip)
            {
                //Return the clip to the ammo pool and empty the ammo pool
                clip = ammo;
                ammo = 0;
            }
        }
    }
}
