﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game_Controller : MonoBehaviour {

    public static Game_Controller gameController;

    public Text healthText;
    public Text livesText;
    public int health;
    public int lives;
    [HideInInspector]
    public Vector3 playerPosition;
    public int ammo;
    public static GameObject[] enemies;
    [HideInInspector]
    public int enemiesKilled = 0;
    public GameObject player;

    int sceneCounter = 1;

    void Awake()
    {
        if (gameController == null)
        {
            DontDestroyOnLoad(gameObject);
            gameController = this;
        }
        else if (gameController != this)
        {
            Destroy(gameObject);
        }
    }
    /// <summary>
    /// Used for instantiation
    /// </summary>
    private void Start()
    {
        UpdateHealth();
        UpdateLives();
        player = GameObject.FindGameObjectWithTag("PLayer");
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
    }

    /// <summary>
    /// Looks for the save button being pressed
    /// </summary>
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            playerPosition = transform.position;
            Save();
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        CheckIfComplete(enemiesKilled);
        if(health <= 0)
        {
            lives--;
            health = 100;
            if(lives <= 0)
            {
                SceneManager.LoadScene("GameOver");
            }
        }
    }

    public void MoveCharacter(Vector3 newPos)
    {
        player.transform.position = playerPosition;
    }

    private void OnSceneLoaded()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
    }

    private void CheckIfComplete(int dead)
    {
        if(enemies.Length == dead)
        {
            int index = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(index + 1);
        }
    }

    public void UpdateHealth()
    {
        healthText.text = "Health: " + health.ToString();
    }

    public void UpdateLives()
    {
        livesText.text = "Lives: " + lives.ToString();
    }

    public void ChangeHealth(int damage)
    {
        health = health - damage;
        UpdateHealth();
    }

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

        PlayerData info = new PlayerData();
        info.health = health;
        info.lives = lives;
        info.playerPos = playerPosition;
        info.currentScene = SceneManager.GetActiveScene().name;

        bf.Serialize(file, info);
        file.Close();
    }
}

[Serializable]
class PlayerData
{
    public int health;
    public int lives;
    public string currentScene;
    public Vector3 playerPos;

}
