﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author: Michael Nelson
//Class Projectile is for giving the projectile damage and to actually hurt targets
public class Projectile : MonoBehaviour {

    //damage the bullet does
    public int damage;

    private void OnCollisionEnter(Collision collision)
    {
        //If you hit the enemy
        if (collision.gameObject.CompareTag("Enemy"))
        {
            //Take damage from wherever the enemy health is stored
            collision.gameObject.GetComponent<Enemy_AI>().health -= damage;

            Destroy(this.gameObject);
        }

        Destroy(this.gameObject, 3f);

        //Destroy the object no matter what it hits
        //Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            print("You got hit");
            Game_Controller.gameController.ChangeHealth(damage);
        }
        Destroy(this.gameObject);
    }
}
