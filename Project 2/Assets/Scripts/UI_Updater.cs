﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Updater : MonoBehaviour {

    public Text health;
    public Text lives;


    public void UpdateHealth(int pHealth)
    {
        health.text = "Health: " + pHealth.ToString();
    }

    public void UpdateLives(string pLives)
    {
        lives.text = "Lives: " + pLives;
    }
}
